BASE16_SHELL="$HOME/.config/base16-shell/base16-solarized.dark.sh"
if [[ $- =~ "i" ]]; then
   [[ -s $BASE16_SHELL ]] && source $BASE16_SHELL
fi
source ~/.config/bash-git-prompt/gitprompt.sh 

# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi
# ANSI color codes
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    #PS1="$HC$FYEL[$FBLE${debian_chroot:+($debian_chroot)}\u$FYEL: $FBLE\w $FYEL]\\$ $RS"
    #PS2="$HC$FYEL&gt; $RS"
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
#    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias watch='watch --color'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias image='feh -B white'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi


# randpw(){ if [ -z "$1" ]; then echo "Usage: randpw <length>"; else < /dev/urandom tr -dc '_A-Z-a-z-0-9.\/[](){}"!$%&=?#+*~;,:.<>|' | head -c ${1:-$1};echo; fi;}


vim(){
	FILE=$1
	if [ -w $FILE ] || [ ! -f $FILE ]
	then
		/usr/bin/vim $@
	else
		sudo /usr/bin/vim $@
	fi
}


function ssh {
	/usr/bin/ssh $@; source ~/.bashrc;
}
function words {
    cat $1 | sed 's: :\n:g' | sed 's:\n\n:\n:g' | wc -l
}

function genpdf {
    pdflatex $1
    bibtex8 $1
    pdflatex $1
    pdflatex $1
    rm *.aux *.out *.log *.bbl *.blg
}

function netmon {
    if [ -z "$1" ]
    then
        echo "Usage: netmon <interface>";
    else
        ip addr show dev $1 2>&1 > /dev/null
        if [ "$?" -eq 0 ]
        then
            while true
            do 
                clear
                ifconfig | sed -n "/$1/,/^$/p" | grep bytes | sed -r "s:.*?(\(.*?\)).*?(\(.*?\)):$1\: Down \1 Up \2:g"
                sleep 1
            done   

        else
            return
        fi
    fi
}
# alias randpw='echo `ping localhost -c 3` `date +%s` `head -c 8 /dev/urandom` `sha256sum /proc/meminfo` | sha256sum | base64 | head -c 16 ; echo'
alias busy="cat /dev/urandom | hexdump -C | grep 'ca fe'" 
alias rd='pass zawiw/webadmin | xargs -i rdesktop -k de -p {} -u Administrator -d zawiw-domain newserver01.zawiw.uni-ulm.de'
alias json='python -m json.tool'
alias :wq='exit'
export DOCKER_CERT_PATH=$HOME/.minikube/certs 
# alias mk="sudo minikube"
alias mk-start="minikube start --vm-driver=none -apiserver-ips 127.0.0.1"
# alias kubectl="sudo kubectl"

# function mk-dashboard() {
#     kubectl proxy &
#     xdg-open http://127.0.0.1:8001/api/v1/namespaces/kube-system/services/http:kubernetes-dashboard:/proxy/
#     fg
# }

bind '"\C-H":unix-word-rubout'
