#!/bin/bash
pwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git pull --recurse-submodules
git submodule init
git submodule update
git submodule status

mkdir -p $HOME/.config
mkdir -p $HOME/.config/fish
mkdir -p $HOME/local

while read line
do
	ln=(${line//	/ })
	rm -rf $HOME/${ln[1]}
	ln -sf $pwd/${ln[0]} $HOME/${ln[1]}
	echo "Softlink set up in" $HOME/${ln[1]}
done < softlinks

exit 0

echo "fish installed? [y|N]"
read fish
if  [ "$fish" = "y" ] || [ "$fish" = "Y" ]
then
	echo "Congretulations, good choice ;)"
else
	echo "Can you get root permission to install fish globally? [y|N]"
	read fish
	if  [ "$fish" = "y" ] || [ "$fish" = "Y" ]
	then
		sudo apt-get install fish
	else
		wget https://gist.githubusercontent.com/masih/10277869/raw/9cdc689c22f7e58c888e7a83cbe5bd41934f26b0/fish_shell_local_install.sh
		bash fish_shell_local_install.sh
		rm fish_shell_local_install.sh
	fi
fi

echo "tmux installed? [y|N]"
read tmux
if  [ "$tmux" = "y" ] || [ "$tmux" = "Y" ]
then
	source $HOME/.bashrc
	exit
fi

echo "Can you get root permission to install tmux globally? [y|N]"
read tmux

if  [ "$tmux" = "y" ] || [ "$tmux" = "Y" ]
then
	sudo apt-get install tmux
else
	wget https://gist.githubusercontent.com/swinkelhofer/0832aba7cc78662e0c9c/raw/7bce03696b5435763d0a35336eaa9faeafeb11d8/tmux_local_install.sh
	bash tmux_local_install.sh
	rm tmux_local_install.sh
fi
source $HOME/.bashrc
