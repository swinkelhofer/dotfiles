#!/bin/sh

GREEN="\e[32m"
YELLOW="\e[34m"
CYAN="\e[36m"
MAGENTA="\e[35m"
RED="\e[31m"
BLUE="\e[34m"
RESET="\e[0m"

function mk_prepare_user() {
    rm -rf ~/.minikube
    sudo cp -r /root/.minikube ~/.minikube
    sudo cp /root/.kube/config ~/.kube/config
    sudo chown -R `id -nu`:`id -nu` ~/.minikube
    sudo chown `id -nu`:`id -nu` ~/.kube/config
    sed -i "s:/root:$HOME:g" ~/.minikube/machines/minikube/config.json
    sed -i "s:/root:$HOME:g" ~/.kube/config
    if ! kubectl get po > /dev/null 2>&1
    then
        echo "${RED}Could not set up k8s for user `id -nu`${RESET}"
    else
        echo "${CYAN}Successfully set up k8s for user `id -nu`${RESET}"
    fi
}

function wait_until_ready() {
    cmd="$1"
    msg="$2"
    success="$3"
    while $cmd > /dev/null 2>&1
    do
        echo "${CYAN}${msg}${RESET}"
        sleep 1
    done
    if [ ! -z "$success" ]; then
        echo "${CYAN}${success}${RESET}"
    fi
}

function mk() {
    if [ "$#" -lt 1 ]; then
        minikube --help
        return
    fi
    if [ "$1" = "start" ]; then
        shift
        rm -rf /tmp/juju* >/dev/null 2>&1
        sudo minikube start --vm-driver=none $@ || return
        wait_until_ready "! kubectl get pods -n kube-system" "Waiting for minikube to be up and running" "Minikube up and running"
        wait_until_ready "kubectl get node | grep NotReady" "Waiting for nodes to be ready" "All nodes up and running"

        sudo minikube addons enable ingress
        sudo minikube addons enable istio-provisioner

        wait_until_ready "! kubectl get istiocontrolplanes.install.istio.io" "Waiting for istio resources to be created" "Ready to deploy istio"
        sudo minikube addons enable istio
        if [ "$UID" -ne "0" ]; then
            mk_prepare_user
        fi
    else
        sudo minikube $@
    fi
}
