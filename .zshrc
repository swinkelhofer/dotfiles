# If you come from bash you might have to change your $PATH.
fpath=(~/.zsh/completion $fpath)
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

BASE16_SHELL="$HOME/.config/base16-shell/base16-monokai.dimmed.sh"
if [[ $- =~ "i" ]]; then
	[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL
fi
# Set name of the theme to load --- if set to "random", it will
	# load a random theme each time oh-my-zsh is loaded, in which case,
		# to know which specific one was loaded, run: echo $RANDOM_THEME
		# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
		# ZSH_THEME="robbyrussell"
ZSH_THEME="avit"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
	# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
	# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
	# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
	# under VCS as dirty. This makes repository status check for large repositories
		# much, much faster.
		# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
	# stamp shown in the history command output.
	# You can set one of the optional three formats:
	# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
	# or set a custom format using the strftime function format specifications,
	# see 'man strftime' for details.
		# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

plugins=(git kube-ps1)
source $ZSH/oh-my-zsh.sh
# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
	# if [[ -n $SSH_CONNECTION ]]; then
		#   export EDITOR='vim'
		# else
			#   export EDITOR='mvim'
			# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
alias ll="ls -lah"
alias cp="~/local/cp -g"
alias mv="~/local/mv -g"
alias watch="watch --color"
alias open="xdg-open"
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
export PATH=$PATH:~/go/bin
function mk-start {
	# sudo chmod g+w /var/lib /var/lib/kubelet
    minikube start --vm-driver=none --apiserver-ips 127.0.0.1
}

alias dd="dd status=progress"
alias k="kubectl"
alias kd="kubectl config use-context dkube"
alias kp="kubectl config use-context pkube"
alias kt="kubectl config use-context tkube"
alias km="kubectl config use-context minikube"

if [ -e ~/.pkubeconfig ]; then
    export KUBECONFIG=~/.kube/config:~/.kube/pkubeconfig:~/.kube/dkubeconfig
else
    export KUBECONFIG=~/.kube/config
fi
compdef _kns kns
function _kns {
	_arguments -C \
		"1: :($(kubectl get namespace -o jsonpath='{.items..metadata.name}'))"
}
kns() {
	if [[ "$1" = "" ]] then
		kubectl get ns
	else
		kubectl config set-context $(kubectl config current-context) --namespace=$1
	fi
}

compdef _kctx kctx
function _kctx {
    _arguments -C \
        "1: :($(kubectl config -o name get-contexts))"
}
kctx() {
	if [[ "$1" = "" ]] then
		kubectl config get-contexts
	else
		kubectl config use-context $1
	fi
}
alias cc="xclip -selection clipboard"

function ssh() {
    /usr/bin/ssh $@
    source ~/.zshrc
}
alias kwtch="watch kubectl get po,svc,configmaps,deployments,netpol,ingress,daemonsets,cronjobs,pv,pvc"
export KUBE_EDITOR=vim
export GO111MODULE=on
bindkey "^H" backward-kill-word
RPROMPT=$RPROMPT'$(kube_ps1) '


autoload -Uz compinit && compinit -i

if ! test -e "kubectl"; then
    kubeoff
fi

if [ -e "/workstation" ]; then
# BEGIN ANSIBLE MANAGED BLOCK
parse() {
     test -r $1 && source $1
}

    parse /workstation/config/files/bashrc.sh
    parse /workstation/config/files/aliases.sh
    parse /workstation/custom/files/custom.sh
# END ANSIBLE MANAGED BLOCK
fi

source ~/.zsh/scripts/*
# Created by `pipx` on 2021-04-07 11:28:04
export PATH="$PATH:/home/swinkelhofer/.local/bin"
