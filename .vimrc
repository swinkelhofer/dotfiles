" set number

imap { {}<LEFT>
imap [ []<LEFT>
imap ( ()<LEFT>
imap < <><LEFT>
inoremap " ""<LEFT>
inoremap ' ''<LEFT>
" vundle {{{1
" needed to run vundle (but i want this anyways)
set shortmess=a
set cmdheight=2

set nocompatible

" vundle needs filtype plugins off
" i turn it on later
filetype plugin indent off
syntax off

" set the runtime path for vundle
set rtp+=~/.vim/bundle/Vundle.vim

" start vundle environment
call vundle#begin()

" list of plugins {{{2
" let Vundle manage Vundle (this is required)
Plugin 'spolu/dwm.vim'
Plugin 'gmarik/Vundle.vim'

" to install a plugin add it here and run :PluginInstall.
" to update the plugins run :PluginInstall! or :PluginUpdate
" to delete a plugin remove it here and run :PluginClean
" 

" YOUR LIST OF PLUGINS GOES HERE LIKE THIS:
Plugin 'bling/vim-airline'
Plugin 'chriskempson/base16-vim'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/syntastic'
Plugin 'tomtom/tcomment_vim'
Plugin 'majutsushi/tagbar'
Plugin 'zxqfl/tabnine-vim'
" add plugins before this
call vundle#end()

" now (after vundle finished) it is save to turn filetype plugins on
filetype plugin indent on
syntax on
set number


let base16colorspace=256  " Access colors present in 256 colorspace
set background=dark
colorscheme base16-default
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE
nmap ö :set mouse=a<CR>
nmap ä :set mouse=""<CR> 
nmap <C-T> <ESC>:silent !ctags -R .<CR>:redraw!<CR>:CtrlPTag<CR>
nmap <F8> :TagbarToggle<CR>
nmap <F7> :set tabstop=4<CR>:%s:    :\t:g<CR>
nmap <C-Y> :.!xclip -selection clipboard -o<CR>
nmap <C-S-Y> :.!xclip -o<CR>
set mouse=a

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
set shell=/bin/bash
set tabstop=4
set shiftwidth=4
set expandtab


au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile

