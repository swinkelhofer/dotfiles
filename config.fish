eval sh $HOME/.config/base16-shell/base16-default.dark.sh
set normal (set_color normal)
set magenta (set_color magenta)
set yellow (set_color yellow)
set green (set_color green)
set red (set_color red)
set gray (set_color -o black)

# Fish git prompt
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_showupstream 'yes'
set __fish_git_prompt_color_branch yellow
set __fish_git_prompt_color_upstream_ahead green
set __fish_git_prompt_color_upstream_behind red

# Status Chars
set __fish_git_prompt_char_dirtystate '⚡'
set __fish_git_prompt_char_stagedstate '→'
set __fish_git_prompt_char_untrackedfiles '☡'
set __fish_git_prompt_char_stashstate '↩'
set __fish_git_prompt_char_upstream_ahead '+'
set __fish_git_prompt_char_upstream_behind '-'

# Alias definitions
alias ll 'ls -alF'
alias la 'ls -A'
alias l 'ls -CF'
alias image 'feh -B white'
alias cd.. 'cd ..'
alias .. 'cd ..'
alias ... 'cd ../..'
# Quatsch :D
alias busy 'cat /dev/urandom | hexdump -C | grep "ca fe"'

# Color definitions
set fish_color_command blue
set fish_color_error red
set fish_color_param white

# Fish welcome text off
set fish_greeting ""

# Prompt override: ✔ user@host:/current/path (gitbranch⚡)$
function fish_prompt
	set last_status $status
    printf "\n"
    if test $status -eq 0
	# if math "$status==0" > /dev/null
		set_color green
		set __fish_status '✔'
	else
		set_color red
		set __fish_status "✘-$status"
	end
	if [ "$SSH_CONNECTION" != "" ]
		set color green
		printf "(ՖՖǶ) "
	end

	printf '%s ' $__fish_status 
	set_color green
	printf '%s@' (whoami) 

	set_color white --bold
	printf '%s'(hostname) 
	set_color normal
	set_color blue
	printf ':%s' (pwd | sed "s:$HOME:~:g")
	set_color normal

	printf '%s$ '  (__fish_git_prompt)

	set_color normal
end

# Open vim with root permissions if necessary
function vim
	if test ! -e $argv[1]
		/usr/bin/vim $argv
	else if test -w $argv[1]
		/usr/bin/vim $argv
	else
		sudo /usr/bin/vim $argv
	end
end

# SSH command override to reset colored shell after end of SSH session
function ssh
    /usr/bin/ssh $argv
	. ~/.config/fish/config.fish;
end

# at <varname> command + predefinitions (omi, zawiw...)
set -x omi 134.60.30.170
set -x zawiw 134.60.79.97
set -x jiffy root@ldap.zawiw.de
set -x jiffyroot "-p 2222 root@ldap.zawiw.de"
function at
	set host (printenv $argv[1])
	set --erase argv[1]
	if [ "$argv" != "" ]
		ssh $host -t "bash -c \"$argv\""
	else
		ssh $host
	end
end

# Tmux alias if necessary
if test -e $HOME/local/bin/tmux
	alias tmux $HOME/local/bin/tmux
end

# Extract archives dependand on filename extension
function extract
    if math (count argv) != 0; and test -e $argv[1]
      switch $argv[1]
        case *.tar.bz2 
				tar xjf $argv[1]
        case *.tar.gz
				tar xzf $argv[1] 
        case *.bz2
				bunzip2 $argv[1] 
        case *.rar
				unrar e $argv[1] 
        case *.gz
				gunzip $argv[1]  
        case *.tar
				tar xf $argv[1]  
        case *.tbz2
				tar xjf $argv[1] 
        case *.tgz
				tar xzf $argv[1] 
        case *.zip
				unzip $argv[1]   
        case *.Z
				uncompress $argv[1]
        case *.7z
				7z x $argv[1]    
        case '*'
				echo "'$argv[1]' cannot be extracted via extract()"
	 	end
     else
         echo "Usage: extract file.<xyz>" 
     end
end

# Callback for !! to last command
function bind_bang
    switch (commandline -t)[-1]
        case "!"
            commandline -t $history[1]; commandline -f repaint
        case "*"
            commandline -i !
    end
end

# Callback for !$ to last command/parameter
function bind_dollar
    switch (commandline -t)[-1]
        case "!"
            commandline -t ""
            commandline -f history-token-search-backward
        case "*"
            commandline -i '$'
    end
end

# Bind ! and $ on callbacks
function fish_user_key_bindings
    bind ! bind_bang
    bind '$' bind_dollar
end
clear
